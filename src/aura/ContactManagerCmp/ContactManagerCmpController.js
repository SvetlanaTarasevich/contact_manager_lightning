({
    init: function (component, event, helper) {
        let page = component.get("v.page") || 1;
        let recordToDisplay = component.find("recordSize").get("v.value");
        component.set('v.mycolumns', [
            {label: 'Contact Name', fieldName: 'Name', type: 'text', sortable: 'true'},
            {label: 'Contact Level', fieldName: 'ContactLevel__c', type: 'text', sortable: 'true'},
            {label: 'Email', fieldName: 'Email', type: 'email', sortable: 'true'},
            {label: 'Account', fieldName: 'Account.Name',  type: 'text', sortable: 'true'},
            {label: 'Owner', fieldName: 'OwnerId', type: 'id', sortable: 'true'},
            {label: 'Created By', fieldName: 'CreatedById', type: 'id', sortable: 'true'},
            {label: 'Created Date', fieldName: 'CreatedDate', type: 'date', sortable: 'true'},
            {label: 'Action', type: 'button', typeAttributes: {iconName: 'utility:delete', label: 'Delete', name: 'delete', disabled: false, value: 'deleteBtn'}}
        ]);
        helper.getData(component, page, recordToDisplay);
    },

    SearchByName: function (component, event, helper) {
        helper.searchHelp(component, event, helper);
    },

    newContact: function (component) {
        component.set("v.newCont", true);
    },

    cancel: function (component) {
        component.set("v.newCont", false);
    },

    onSave: function (component, event, helper) {
        component.set("v.newCont", false);
        helper.getData(component, event, helper);
    },

    delCont: function (component, event, helper) {
        helper.helpDel(component, event, helper);
    },

    updateColumnSorting: function (component, event, helper) {
        let fieldName = event.getParam('fieldName');
        let sortDirection = event.getParam('sortDirection');
        component.set("v.sortedBy", fieldName);
        component.set("v.sortedDirection", sortDirection);
        helper.sortData(component, fieldName, sortDirection);
    },

    navigate: function (component, event, helper) {
        let page = component.get("v.page") || 1;
        let direction = event.getSource().get("v.label");
        let recordToDisplay = component.find("recordSize").get("v.value");
        page = direction === "Previous Page" ? (page - 1) : (page + 1);
        helper.getData(component, page, recordToDisplay);
    },

    onSelectChange: function (component, event, helper) {
        let page = 1;
        let recordToDisplay = component.find("recordSize").get("v.value");
        helper.getData(component, page, recordToDisplay);
    }
})