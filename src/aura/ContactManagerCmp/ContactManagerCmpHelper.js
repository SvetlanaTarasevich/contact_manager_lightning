({
    getData: function (component, page, recordToDisplay) {
        let action = component.get("c.getContacts");
        action.setParams({
            "pageNumber": page,
            "recordToDisplay": recordToDisplay
        });
        action.setCallback(this, function (response) {
            let records = response.getReturnValue();
            let state = response.getState();
            if (state === "SUCCESS") {
                records.contacts.forEach(function (record) {
                    record.linkName = '/' + record.Id;
                });
                console.log(records.contacts);
                component.set("v.mydata", records.contacts);
                component.set("v.page", records.page);
                component.set("v.total", records.total);
                component.set("v.pages", Math.ceil(records.total / recordToDisplay));
            } else if (state === "ERROR") {
                let errors = response.getError();
                console.error(errors);
            }
        });
        $A.enqueueAction(action);
    },

    searchHelp: function (component) {
        let nameSegment = component.find("search").get("v.value");
        let action = component.get("c.searchContactByNameBack");
        action.setParams({
            nameSegment: nameSegment
        });
        action.setCallback(this, function (response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                console.log(response.getReturnValue());
                component.set("v.mydata", response.getReturnValue());
            } else if (state === "ERROR") {
                let errors = response.getError();
                console.error(errors);
            }
        });
        $A.enqueueAction(action);
    },

    helpDel: function (component, event, helper) {
        let row = event.getParam('row');
        let action = component.get("c.deleteContactById");
        action.setParams({
            contid: row.Id
        });

        action.setCallback(this, function (response) {
            let state = response.getState();
            if (state === "SUCCESS") {
                location.reload();
            } else if (state === "ERROR") {
                let errors = response.getError();
                console.error(errors);
            }
        });
        $A.enqueueAction(action);
    },

    sortData: function (component, fieldName, sortDirection) {
        let data = component.get("v.mydata");
        let reverse = sortDirection !== 'asc';
        data.sort(this.sortBy(fieldName, reverse))
        component.set("v.mydata", data);
    },

    sortBy: function (field, reverse, primer) {
        let key = primer ? function (x) {
            return primer(x[field])
        } : function (x) {
            return x[field]
        };
        reverse = !reverse ? 1 : -1;
        return function (a, b) {
            return a = key(a), b = key(b), reverse * ((a > b) - (b > a));
        }
    }
})