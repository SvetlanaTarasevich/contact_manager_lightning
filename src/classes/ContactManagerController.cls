public with sharing class ContactManagerController {

        public class ContactPagerWrapper {
            @AuraEnabled public Integer pageSize { get; set; }
            @AuraEnabled public Integer page { get; set; }
            @AuraEnabled public Integer total { get; set; }
            @AuraEnabled public List<Contact> contacts { get; set; }
        }

        @AuraEnabled
        public static ContactPagerWrapper getContacts(Decimal pageNumber, Integer recordToDisplay) {
            Integer pageSize = recordToDisplay;
            Integer offset = ((Integer) pageNumber - 1) * pageSize;
            ContactPagerWrapper pageWrapper = new ContactPagerWrapper();
            pageWrapper.pageSize = pageSize;
            pageWrapper.page = (Integer) pageNumber;
            pageWrapper.total = [SELECT COUNT() FROM Contact];
            pageWrapper.contacts = [SELECT  Id,
                                            Name,
                                            Email,
                                            Account.Name,
                                            Owner.Name,
                                            CreatedBy.Name,
                                            Contact_Level__c,
                                            CreatedDate
                                    FROM Contact
                                    ORDER BY Name DESC
                                    LIMIT :recordToDisplay
                                    OFFSET :offset];
            return pageWrapper;
        }

        @AuraEnabled
        public static List<Contact> searchContactByNameBack(String nameSegment) {
            String nameSegmentSoql = '%' + nameSegment + '%';
            List<Contact> contactList = [SELECT Id,
                                                Name,
                                                Email,
                                                Account.Name,
                                                Owner.Name,
                                                CreatedBy.Name,
                                                Contact_Level__c,
                                                CreatedDate
                                        FROM Contact
                                        WHERE Name
                                        LIKE :nameSegmentSoql];
            return contactList;

        }

        @AuraEnabled
        public static void deleteContactById(String contid) {
            Contact delContact = [SELECT Id,
                                         Name,
                                         Email,
                                         Account.Name,
                                         Owner.Name,
                                         CreatedBy.Name,
                                         Contact_Level__c,
                                         CreatedDate
                                 FROM Contact
                                 WHERE Id = : contid];
            delete delContact;
        }
    }